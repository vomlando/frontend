export const rules = {
  mail: [
    (v) => !!v || 'E-Mail Adresse ist Pflicht',
    (v) =>
      /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(
        v
      ) || 'E-Mail Adresse ist ungültig',
  ],
  password: [
    (v) => !!v || 'Passwort ist Pflicht',
    (v) => v.length > 5 || 'Passwort muss mindestens 6 Zeichen lang sein',
  ],
}
