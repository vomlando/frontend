FROM node:alpine

RUN mkdir -p /app
WORKDIR /app

COPY package.json /app

ENV NODE_ENV=production
ENV LOAD_DEV=false
ENV NUXT_PORT=5000
ENV NUXT_HOST=0.0.0.0

RUN npm install --production

COPY . /app

EXPOSE 5000

ENTRYPOINT ["/usr/local/bin/npm", "start"]