module.exports = {
  mode: 'universal',
  /*
   ** Headers of the page
   */
  head: {
    titleTemplate: '%s - ' + 'VomLando',
    title: 'VomLando' || '',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      {
        hid: 'description',
        name: 'description',
        content: process.env.npm_package_description || '',
      },
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }],
  },
  /*
   ** Customize the progress-bar color
   */
  loading: { color: '#607D8B' },
  /*
   ** Plugins to load before mounting the App
   */
  plugins: [{ src: '~/plugins/markercluster.js', mode: 'client' }],
  /*
   ** Nuxt.js dev-modules
   */
  buildModules:
    process.env.LOAD_DEV === 'false'
      ? []
      : ['@nuxtjs/eslint-module', '@nuxtjs/vuetify'],

  polyfill: {
    features: [
      {
        require: 'intersection-observer',
        detect: () => 'IntersectionObserver' in window,
      },
    ],
  },

  /*
   ** Nuxt.js modules
   */
  modules: [
    '@nuxtjs/axios',
    '@nuxtjs/pwa',
    '@nuxtjs/auth',
    'nuxt-leaflet',
    'nuxt-polyfill',
    'nuxt-webfontloader',
  ],

  auth: {
    strategies: {
      local: {
        endpoints: {
          login: {
            url: '/auth/login',
            method: 'post',
            propertyName: 'token',
          },
          user: { url: '/admin/', method: 'get', propertyName: 'user' },
        },
      },
    },
  },

  pwa: {
    manifest: {
      name: 'VomLando',
      short_name: 'VomLando',
      start_url: '..',
      lang: 'de-DE',
      display: 'standalone',
      background_color: '#fafafa',
      description: 'Platform für regionale Lebensmittel Erzeuger',
      theme_color: '#8bc34a',
    },
  },

  workbox: {
    runtimeCaching: [
      {
        urlPattern: 'https://cdnjs.cloudflare.com/*',
        handler: 'staleWhileRevalidate',
        method: 'GET',
        cacheableResponse: { statuses: [0, 200] },
      },
      {
        urlPattern: 'https://res.cloudinary.com/.*',
        handler: 'staleWhileRevalidate',
        method: 'GET',
        cacheableResponse: { statuses: [0, 200] },
      },
      {
        urlPattern: 'vomlando.de/(?!admin).*',
        handler: 'staleWhileRevalidate',
        method: 'GET',
        cacheableResponse: { statuses: [0, 200] },
      },
      {
        urlPattern: 'https://api.vomlando.de/admin/.*',
        handler: 'networkFirst',
        method: 'GET',
        cacheableResponse: { statuses: [0, 200] },
      },
    ],
  },
  /*
   ** Axios module configuration
   ** See https://axios.nuxtjs.org/options
   */
  axios: {
    baseURL: 'http://backend-service:5000/',
    browserBaseURL: 'https://api.vomlando.de/',
  },
  /*
   ** vuetify module configuration
   ** https://github.com/nuxt-community/vuetify-module
   */
  vuetify: {
    customVariables: ['~/assets/variables.scss'],
    theme: {
      themes: {
        light: {
          primary: '#8bc34a', // colors.lightGreen.base,
          accent: '#424242', // colors.grey.darken3,
          secondary: '#ff8f00', // colors.amber.darken3,
          info: '#26a69a', // colors.teal.lighten1,
          warning: '#ffc107', // colors.amber.base,
          error: '#dd2c00', // colors.deepOrange.accent4,
          success: '#00e676', // colors.green.accent3
        },
      },
    },
    treeShake: true,
    icons: {
      iconfont: 'mdiSvg',
    },
    defaultAssets: {
      icons: false,
    },
  },

  webfontloader: {
    google: {
      families: ['Roboto:100,300,400,500,700,900&display=swap'],
    },
  },

  pageTransition: 'slide-y-reverse-transition',
  /*
   ** Build configuration
   */
  build: {
    cache: true,
    /*
     ** You can extend webpack config here
     */
    extend(config, ctx) {},
  },
}
