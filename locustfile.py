from locust import HttpLocust, TaskSet, task
from random import randrange


class MyTaskSet(TaskSet):
    @task(1)
    def index(self):
        self.client.get("/")

    @task(2)
    def producer(self):
        self.client.get("/karte/")

    @task(3)
    def categories(self):
        self.client.get("/karte/category-" + str(randrange(0, 100)))

    @task(4)
    def detailView(self):
        self.client.get("/hersteller/mock-user-" + str(randrange(0, 1000)))


class MyLocust(HttpLocust):
    task_set = MyTaskSet
    min_wait = 5000
    max_wait = 15000
