export const state = () => ({ location: null })

export const mutations = {
  update(state, location) {
    state.location = location
  },
}

export const getters = {
  location(state) {
    return state.location
  },
}
