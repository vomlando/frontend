export const state = () => ({
  message: null,
  type: null,
})

export const mutations = {
  notify(state, message) {
    state.message = message
    state.type = null
  },
  error(state, message) {
    state.message = message || 'Ups, etwas ist schief gelaufen ¯\\_(ツ)_/¯'
    state.type = 'error'
  },
  success(state, message) {
    state.message = message
    state.type = 'success'
  },
}
